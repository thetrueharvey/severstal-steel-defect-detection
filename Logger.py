"""
Logger for storing and reporting results
"""
#%% Setup
import pandas as pd
import numpy  as np

import torch
import torch.nn.functional as F


#%% Accuracy class
class Logger:
    def __init__(self, accuracy):
        self.accuracy_fn = accuracy

        # Storage
        self.epochs            = []
        self.loss              = []
        self.accuracy          = []
        self.positive_accuracy = []
        self.negative_accuracy = []

    def __call__(self, out, labels, epoch, loss):
        # Accuracy
        accuracy = self.accuracy_fn(out, labels)

        self.accuracy.append(accuracy["average"])
        self.positive_accuracy.append(accuracy["positive"])
        self.negative_accuracy.append(accuracy["negative"])

        # Loss
        self.loss.append(loss)

    def print_latest(self, n=10):
        # Accuracy
        accuracy          = np.nanmean(np.concatenate(self.accuracy[-n:]))
        positive_accuracy = np.nanmean(np.concatenate(self.positive_accuracy[-n:]))
        negative_accuracy = np.nanmean(np.concatenate(self.negative_accuracy[-n:]))

        # Loss
        loss = np.nanmean(np.concatenate(self.loss[-n:]))

        return "Loss: {0:.3f} Accuracy: {1:.3f} Positive Accuracy: {2:.3f} Negative Accuracy: {3:.3f}".format(loss, accuracy, positive_accuracy, negative_accuracy)

    #def print_epoch(self):