"""
Custom Acc function
"""
#%% Setup
import torch
import torch.nn.functional as F

#%% Loss class
class MaskAccuracy:
    def __init__(self):
        pass

    def __call__(self, predictions, labels):
        # Index
        positive_predictions = [prediction[label >= 0.5] for prediction, label in zip(predictions, labels)]
        negative_predictions = [prediction[label < 0.5] for prediction, label in zip(predictions, labels)]

        # Calculate loss components per element
        positive_accuracy = torch.stack([(F.sigmoid(prediction) >= 0.5).float().mean()
                                          for prediction in positive_predictions
                                        ]
                                       )
        negative_accuracy = torch.stack([(F.sigmoid(prediction) < 0.5).float().mean()
                                         for prediction in negative_predictions
                                        ]
                                       )

        # Add components per element
        accuracy = torch.stack([mean_accuracy(positive, negative)
                                for positive, negative in zip(positive_accuracy, negative_accuracy)
                               ]
                              )

        # Average and return
        return { "average" : accuracy.numpy()
               , "positive": positive_accuracy.numpy()
               , "negative": negative_accuracy.numpy()
               }

def mean_accuracy(a, b):
    if a != a:
        return b
    if b != b:
        return a

    return (a + b) / 2