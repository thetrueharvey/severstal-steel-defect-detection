"""
Implementation of MobileNet V2
Based on
"""
#%% Setup
import numpy as np

import torch
import torch.nn            as nn
import torch.nn.functional as F

from torchtools.nn import SimpleSelfAttention

#%% MobileNet V2 Class
class MobileNetV2(nn.Module):
    def __init__( self
                , input_shape=[1,224,224]
                , width_mult =1.0
                , name       ="MobileNetV2 Model"
                ):
        """
        TODO: Update documentation
        :param n_class:
        :param input_shape:
        :param width_mult:
        :param name:
        """
        super(MobileNetV2, self).__init__()

        # Class attributes
        self.name = name

        # Network architecture
        interverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [6, 24, 2, 2],
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]

        # Create the first layer
        self.layers = nn.ModuleList([Conv2D_BN(in_features=input_shape[0], out_features=32, stride=1)])

        # Build the residual blocks
        in_channels = self.layers[0].conv.out_channels
        for t, c, n, s in interverted_residual_setting:
            out_channels = make_divisible(c * width_mult) if t > 1 else c
            for i in range(n):
                if i == 0:
                    self.layers.append(InvertedResidual( in_features =in_channels
                                                       , out_features=out_channels
                                                       , stride      =s
                                                       , expand_ratio=t
                                                       )
                                     )
                else:
                    self.layers.append(InvertedResidual( in_features =in_channels
                                                       , out_features=out_channels
                                                       , stride      =1
                                                       , expand_ratio=t
                                                       )
                                      )

                in_channels = out_channels

            #self.layers.append(SimpleSelfAttention(out_channels))

        # Build the remaining convolutional layer
        self.layers.append(Conv2D_BN( in_features =in_channels
                                    , out_features=make_divisible(1280 * width_mult) if width_mult > 1.0 else 1280
                                    , kernel_size =1
                                    , stride      =1
                                    , padding     =0
                                    )
                          )

        # Upsample
        self.upsample = nn.Upsample(scale_factor=16)

        # Add prediction layer
        self.prediction = nn.Conv2d(in_channels=1280, out_channels=1, kernel_size=1)

        # Initialize weights
        self._init_weight()

    # Forward function
    def forward(self, x):
        # Pass through the convolutional layers
        for layer in self.layers:
            x = layer(x)

        # Classification
        x = self.upsample(x)
        #x = Mish(x)
        x = F.leaky_relu(x)
        x = self.prediction(x)

        return torch.clamp(x.cpu().float(), min=-1e-5, max=1e5)

    # Weight initialization
    def _init_weight(self):
        for m in self.modules():
            #print(m)
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight)
                m.bias.data.zero_()


#%% Utilities
class Conv2D_BN(nn.Module):
    def __init__(self, in_features, out_features, kernel_size=3, stride=1, padding=1, groups=1, activation=True):
        """
        Convolution followed by Batch Normalization, with optional Mish activation
        :param in_features:
        :param out_features:
        :param kernel_size:
        :param stride:
        :param padding:
        :param groups:
        :param activation:
        """
        super(Conv2D_BN, self).__init__()

        # Class parameters
        self.activation = activation

        # Convolution and BatchNorm class
        self.conv = nn.Conv2d( in_channels =in_features
                             , out_channels=out_features
                             , kernel_size =kernel_size
                             , stride      =stride
                             , padding     =padding
                             , groups      =groups
                             , bias        =True
                             )

        self.bn = nn.BatchNorm2d(num_features=out_features, momentum=0.9)

    def forward(self, x):
        x = self.conv(x)
        if self.activation:
            #x = Mish(x)
            x = F.leaky_relu(x)

        x = self.bn(x)

        return x


def make_divisible(x, divisible_by=8):
    return int(np.ceil(x * 1. / divisible_by) * divisible_by)


class InvertedResidual(nn.Module):
    def __init__( self
                , in_features
                , out_features
                , stride
                , expand_ratio
                ):
        """
        MobileNet Inverted Residual block
        :param in_features:
        :param out_features:
        :param stride:
        :param expand_ratio:
        """
        super(InvertedResidual, self).__init__()

        # Input validation
        assert stride in [1, 2]

        # Class parameter
        self.stride = stride

        # Calculate the hidden dimension
        hidden_dim = int(in_features * expand_ratio)

        # Determine whether to use a residual connection
        self.res_connect = self.stride == 1 and in_features == out_features

        if expand_ratio == 1:
            conv_1 = Conv2D_BN( in_features =hidden_dim
                              , out_features=hidden_dim
                              , kernel_size =3
                              , stride      =stride
                              , padding     =1
                              , groups      =hidden_dim
                              )

            conv_2 = Conv2D_BN( in_features =hidden_dim
                              , out_features=out_features
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              , activation  =False
                              )

            self.layers = nn.ModuleList([conv_1, conv_2])

        else:
            conv_1 = Conv2D_BN(in_features  =in_features
                              , out_features=hidden_dim
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              )

            conv_2 = Conv2D_BN( in_features =hidden_dim
                              , out_features=hidden_dim
                              , kernel_size =3
                              , stride      =stride
                              , padding     =1
                              , groups      =hidden_dim
                              )

            conv_3 = Conv2D_BN( in_features =hidden_dim
                              , out_features=out_features
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              , activation  =False
                              )

            self.layers = nn.ModuleList([conv_1, conv_2, conv_3])

    def forward(self, x):
        if self.res_connect:
            return x + self._block_forward(x, self.layers)
        else:
            return self._block_forward(x, self.layers)

    @staticmethod
    def _block_forward(x, block):
        for layer in block:
            x = layer(x)

        return x

# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))
