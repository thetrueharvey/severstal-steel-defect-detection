"""
Custom dataset class
"""

#%% Setup
from glob import glob

import os
import torch
import torchvision.transforms.functional as FT

import numpy  as np
import pandas as pd

from PIL       import Image

#%% Dataset class
class SeverstalDataset(object):
    def __init__( self
                , image_folder    : str
                , mask_folder     : str
                , df              : pd.DataFrame
                , feature         : int
                , train_transform : list = None
                , test_transform  : list = None
                ):
        """
        Class for creating a dataset for training
        """
        # Class attributes
        self.image_folder    = image_folder
        self.mask_folder     = mask_folder
        self.df              = df
        self.feature         = feature
        self.train_transform = train_transform
        self.test_transform  = test_transform

        # Lists
        self.images, self.labels = self._get_data_lists()

        # Indices
        indices = np.arange(len(self.images))
        np.random.shuffle(indices)

        # Training and test split
        self.train_indices = indices[:int(len(indices) * 0.7)]
        self.test_indices  = indices[int(len(indices) * 0.7):]

        # Instantiate mode
        self.train()


    def __len__(self):
        return len(self._active_indices)

    def __getitem__(self, idx):
        # Load the image
        img = Image.open(self.image_folder + "/" + self.images[self._active_indices[idx]])

        # Load the label
        label = self.labels[self._active_indices[idx]]

        # Check if the label is negative
        if label == "":
            label = Image.fromarray(np.zeros((img.height, img.width)))
        else:
            label = Image.open(self.mask_folder + "/" + label)

        # Apply transformations
        img, label = self._active_transform(img, label)

        return {"image": img, "label": label}

    def eval(self):
        self.mode = "eval"
        self._active_indices   = self.test_indices
        self._active_transform = self.test_transform

    def train(self):
        self.mode = "train"
        self._active_indices   = self.train_indices
        self._active_transform = self.train_transform

    def _get_data_lists(self):
        # Fetch the positive labels
        positive_labels = self.df[self.df["class"] == self.feature]

        # Fetch the negative labels and determine which images contain positive examples
        negative_labels = self.df[self.df["class"] != self.feature]
        negative_labels = negative_labels[~negative_labels["imageId"].isin(positive_labels["imageId"])]

        # Hence fetch positive images and masks (negative masks will be handled at data generation)
        positive_images = positive_labels["imageId"]
        positive_masks  = positive_labels["maskId"]

        # Fetch a equal amount of negative images
        negative_images = negative_labels["imageId"].sample(n=len(positive_images), random_state=0)
        
        # Create the negative masks as empty file paths
        negative_masks = np.repeat("", len(negative_images))
        
        # Concatenate and shuffle
        return [ pd.concat([ positive_images
                           , negative_images
                           ]
                          ).sample(frac=1.0, random_state=0).values
               , pd.concat([ positive_masks
                           , pd.Series(negative_masks)
                           ]
                          ).sample(frac=1.0, random_state=0).values
               ]
