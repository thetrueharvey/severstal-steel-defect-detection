"""
Main training script
"""
#%% Setup
import pandas as pd
import torch

from torch.utils.data         import DataLoader
from torch.nn                 import BCEWithLogitsLoss
from torchvision              import transforms
from torchtools.optim         import RangerLars
from torch.optim.lr_scheduler import ReduceLROnPlateau

import Transforms as T

from Dataset          import SeverstalDataset
from nets.MobileNetV2 import MobileNetV2
from Utils            import train
from Loss             import MaskLoss, MaskLoss2
from Accuracy         import MaskAccuracy
from Logger           import Logger


#%% Dataset
# Transform
train_transform = T.Compose([ T.ChooseOne([ #T.RandomNoise(decay=1/600e3, decay_stop=0.8, probability=0.5)
                                            T.RandomErasures(n_erasures=[1,3], h=[0.15, 0.4], w=[0.15, 0.4], decay=1/600e3, decay_stop=0.8, probability=1.0)
                                          , T.RandomAffine(decay=1/600e3, decay_stop=0.5, probabilities=[0.5,0.5,0.5,0.5,0.5,0.5])
                                            #, T.RandomCrop(decay=1/600e3, decay_stop=0.3, probability=0.5)
                                          , T.RandomColourJitter(decay=1/600e3, decay_stop=0.1, probabilities=[0.4,0.4,0.4,0.4,0.4])
                                          , T.RandomFlip(h_prob=0.8, v_prob=0.8)
                                          ]
                                         )
                            , T.Resize(224, 224)
                            , T.Grayscale()
                            ]
                           )

test_transform = T.Compose([ T.Resize(224, 224)
                           , T.Grayscale()
                           ]
                          )

# Training - test split
df = pd.read_csv("dataset/train labels.csv").sample(frac=1.0)
df_train = df.iloc[:int(len(df) * 0.7)]
df_test  = df.iloc[int(len(df) * 0.7):]

# Dataset
ds_train = SeverstalDataset( image_folder="dataset/processed_images"
                           , mask_folder ="dataset/processed_masks"
                           , df          =df_train
                           , feature     =1
                           , train_transform=train_transform
                           , test_transform =test_transform
                           )

ds_test = SeverstalDataset( image_folder="dataset/processed_images"
                          , mask_folder ="dataset/processed_masks"
                          , df          =df_test
                          , feature     =1
                          , train_transform=train_transform
                          , test_transform =test_transform
                          )

# Loader
train_loader = DataLoader( dataset   =ds_train
                         , batch_size=4
                         , shuffle   =True
                         )

test_loader = DataLoader( dataset   =ds_test
                        , batch_size=4
                        , shuffle   =False
                        )

# Test the dataset
if False:
    sample = ds.__getitem__(0)

    img  = transforms.functional.to_pil_image(sample["image"])
    mask = transforms.functional.to_pil_image(sample["label"])

    img.show()
    mask.show()

#%% Network
net       = MobileNetV2(input_shape=(1,224,224))
#loss_fn   = BCEWithLogitsLoss()
loss_fn   = MaskLoss()
#loss_fn   = MaskLoss2()
accuracy  = MaskAccuracy()
optimizer = RangerLars(params=net.parameters(), lr=0.001)
#optimizer = torch.optim.SGD(params=net.parameters(), lr=0.001)
#optimizer = torch.optim.Adam(params=net.parameters(), lr=0.000001)
#optimizer = torch.optim.RMSprop(params=net.parameters(), lr=0.0001)
scheduler = ReduceLROnPlateau( optimizer=optimizer
                             , mode     ="max"
                             , factor   =0.8
                             , patience =5
                             , verbose  =1
                             , min_lr   =1e-8
                             , cooldown =0
                             )

net.cuda()

#%% Training
train( name          ="MobileNetV2"
     , train_loader  =train_loader
     , test_loader   =test_loader
     , net           =net
     , loss_fn       =loss_fn
     , logger        =Logger
     , accuracy_fn   =accuracy
     , optimizer     =optimizer
     , scheduler     =scheduler
     , scheduler_type="per epoch"
     , epoch_end     =200
     , early_stopping=200
     , half          =True
     )
