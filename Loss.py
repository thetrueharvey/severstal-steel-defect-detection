"""
Custom Loss function
"""
#%% Setup
import torch
import torch.nn as nn

#%% Loss class
class MaskLoss(nn.Module):
    def __init__(self):
        super(MaskLoss, self).__init__()

        # Instantiate losses
        self.positive_loss = nn.MSELoss()
        self.negative_loss = nn.MSELoss()

    def forward(self, predictions, labels):
        # Index
        positive_predictions = [prediction[label >= 0.5] for prediction, label in zip(predictions, labels)]
        negative_predictions = [prediction[label < 0.5] for prediction, label in zip(predictions, labels)]

        # Calculate loss components per element
        positive_loss = [self.positive_loss(torch.sigmoid(prediction), torch.ones_like(prediction)) for prediction in positive_predictions]
        negative_loss = [self.negative_loss(torch.sigmoid(prediction), torch.zeros_like(prediction)) for prediction in negative_predictions]
        
        # Add components per element
        loss = torch.stack([zero_if_nan(positive * 3) + zero_if_nan(negative)
                            for positive, negative in zip(positive_loss, negative_loss)
                           ]
                          )

        # Average and return
        return { "mean"     : loss.mean()
               , "component": loss.detach().numpy()
               }


class MaskLoss2(nn.Module):
    def __init__(self):
        super(MaskLoss2, self).__init__()

        # Instantiate loss
        self.loss = nn.MSELoss(reduction="none")

    def forward(self, predictions, labels):
        # Per sample loss
        sample_loss = self.loss(torch.sigmoid(predictions), labels).mean(dim=[1,2,3])

        # Average and return
        return { "mean"     : sample_loss.mean()
               , "component": sample_loss.detach().numpy()
               }

def zero_if_nan(x):
    if x != x:
        return 0.
    else:
        return x