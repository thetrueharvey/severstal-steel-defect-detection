"""
Collection of utility functions for model training and building
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch
import torch.nn.functional as F

#%% Function
def train( name
         , train_loader
         , test_loader
         , net
         , loss_fn
         , optimizer
         , scheduler
         , logger
         , accuracy_fn
         , scheduler_type="per batch"
         , epoch_start   =1
         , epoch_end     =100
         , half          =False
         , early_stopping=10
         , start_accuracy=0
         ):
    """

    :param name:
    :param dataset:
    :param loader:
    :param net:
    :param loss_fn:
    :param optimizer:
    :param epoch_start:
    :param epoch_end:
    :param half:
    :param early_stopping:
    :return:
    """
    train_logger = logger(accuracy=accuracy_fn)
    test_logger  = logger(accuracy=accuracy_fn)

    # Track the best accuracy and the epochs since testing accuracy improved
    best_acc = start_accuracy
    stationary_epochs = 0

    # Convert to half precision if desired
    if half:
        net.half()

    for epoch in range(epoch_start, epoch_end + 1):
        # Training
        net.train()
        for i, sample in enumerate(train_loader):
            optimizer.zero_grad()

            # Forward pass
            out = net(sample["image"].half().cuda())

            # Loss
            loss_ = loss_fn(out, sample["label"])

            # Back propogation
            loss_["mean"].backward()
            optimizer.step()

            # Scheduler update
            if scheduler_type == "per batch":
                scheduler.step()

            # Logger
            train_logger(out, sample["label"], epoch, loss_["component"])

            # Print a summary every 10 minibatches
            if (i + 1) % 10 == 0:
                print("Training Epoch: {} {}".format(epoch, train_logger.print_latest()), end='\r')

        # Testing
        net.eval()
        with torch.no_grad():
            for i, sample in enumerate(test_loader):
                # Forward pass
                out = net(sample["image"].half().cuda())

                # Loss
                loss_ = loss_fn(out, sample["label"])

                # Logger
                test_logger(out, sample["label"], epoch, loss_["component"])

        # Update learning rate
        if scheduler_type == "per epoch":
            scheduler.step(np.nanmean(np.concatenate(test_logger.accuracy[-len(test_loader):])))

        print("Testing Epoch: {} {}".format(epoch, test_logger.print_latest()))

        # Cleanup
        torch.cuda.empty_cache()

        # Save a distinct checkpoint every 5 epochs
        torch.save({ "epoch": epoch
                   , "model_state_dict"    : net.state_dict()
                   , "optimizer_state_dict": optimizer.state_dict()
                   , "train_log"           : train_logger
                   , "test_log"            : test_logger
                   }
                  , "checkpoints/{} Weights.tar".format(name)
                  )

        if np.nanmean(np.concatenate(test_logger.accuracy[-len(test_loader):])) > best_acc:
            best_acc = np.nanmean(np.concatenate(test_logger.accuracy[-len(test_loader):]))

            torch.save({ "epoch": epoch
                       , "model_state_dict"    : net.state_dict()
                       , "optimizer_state_dict": optimizer.state_dict()
                       , "train_log"           : train_logger
                       , "test_log"            : test_logger
                       }
                      , "checkpoints/{} Best Weights.tar".format(name)
                      )

            stationary_epochs = 0
        else:
            stationary_epochs += 1

    return best_acc, epoch
